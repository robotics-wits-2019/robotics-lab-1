#!/bin/bash
# Clear the screen
clear
 
# Read input using read command
# Workspace 
read -p "What would you like to call your workspace? " workspace_name
mkdir -p ~/ROS_projects/$workspace_name/src 
cd ~/ROS_projects/$workspace_name/src
echo "Folder for $workspace_name created in folder ROS_projects"

# Packages 
read -p "What would you like to call your package? " package_name
## Standard ROS packages to add
ROS_Packages=""
flag=""
while [[ !("$flag" == "x" || "$flag" == "X") ]]
do
	read -p "What standard ROS packages would you like to include? (enter 'x' to continue)" input_package
	flag="$input_package"
	if [[ !("$flag" == "x" || "$flag" == "X") ]]
	then
		ROS_Packages="$ROS_Packages $input_package"
	fi 
done 
catkin_create_pkg $package_name $ROS_Packages 
cd ..

#make
catkin_make
source devel/setup.bash

#scripts 
roscd $package_name
mkdir scripts
cd scripts
touch script_1.py
chmod +x *.py



#You need to set up the environment in ALL terminals (note: order is important)
#source /opt/ros/kinetic/setup.bash 	# ALWAYS do this 
#source ~/ROS_projects/<workspace_name>/devel/setup.bash

# Need to make all node files executable 
#chmod u+x ~/<worspace>/src/<package>/src/<ROS_node>.py



### Important comands ###
# roscore 	# start ROS 

# rostopic 	# used to interact with topics
# rosrun 	# used to launch nodes from packages -> can be python / java / c++ ... nodes 
# roslaunch # launches a world file -> starts multiple nodes 

# rostopic list 	# list all available topics 


### Debugging ### 
# http://wiki.ros.org/ROS/Tutorials/UnderstandingTopics

# rosrun rqt_graph rqt_graph	creates a dynamic graph of what's going on in the system

# rostopic bw     display bandwidth used by topic
# rostopic echo   print messages to screen
# rostopic hz     display publishing rate of topic    
# rostopic list   print information about active topics
# rostopic pub    publish data to topic
# rostopic type   print topic type


# rosrun rqt_plot rqt_plot	displays a scrolling time plot of the data published on topics



