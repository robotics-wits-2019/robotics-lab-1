#!/bin/bash
# Clear the screen
clear

source /opt/ros/kinetic/setup.bash
source ~/ROS_projects/turtlecontroller_ws/devel/setup.bash

rosrun turtlecontroller_pkg move_sequence.py
